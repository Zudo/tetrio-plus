# TETR.IO PLUS
TETR.IO customization toolkit for modifying block skins, sound effects, music, backgrounds and more. Additionally, create dynamic experiences using the music graph, create and load custom maps with the map editor, and play on mobile with completely customizable touch controls.

TETR.IO PLUS is a **third-party game modification** for TETR.IO.

See the [wiki](https://gitlab.com/UniQMG/tetrio-plus/-/wikis/home) for
more information.

## Warnings
- This software is not associated with or created by TETR.IO or osk.
- Do **not** report issues to TETR.IO/osk while TETR.IO PLUS is installed.
- This extension can potentially break your game. If your game fails to load or you encounter other bugs, try removing the addon.
- Chromium-based browsers are not supported as [they lack](https://bugs.chromium.org/p/chromium/issues/detail?id=487422) an [important API](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/filterResponseData) used by this project.
